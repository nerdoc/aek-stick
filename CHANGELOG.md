# Changelog

Alle nennenswürdigen Änderungen werden in dieser Datei festgehalten.

Die Versionierung folgt dem *Semantic Versioning*-Schema.
Das Format basiert auf [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

Änderungen:

## [1.3.0] - 2018-03-11
- Update der Dokumentation
- Wechsel von Coffeescript zu Js, soweit sinnvoll
- Bilder ohne Personen dahinter bekommen einen Avatar
- Fix für Renderfehler
- Allgemeine Vereinfachungen

## [1.2.0] - 2018-02-25
- Verbessetung/Fix mk-package

## [1.1.5] - 2018-02-25
- Fix für falsche Changelog-Inaltgenerierung

## [1.1.4] - 2018-02-25
- Verbesserung git-Handling bei mk-package
- Lizenzbeschreibungen fürs Projekt hinzugefügt
- Wechsel von Jade zu Pug
- viele kleine Änderungen
- Netzwerkoption von brunch

## [1.1.3] - 2018-01-28
- mk-package.sh verbessert
- Christian Gonzalez: neue Emailadresse
- CHANGELOG.md erstellt

## [1.0.9] - 2018-01-28

- Korrekturen von kleineren Fehlern
- :md tags zu :marked geändert, +jstransformer-marked als Abhängigkeit
- Dokumentation der Installation und Erstellung von Inhalt
- AGPL als Lizenz hinzugefügt
- node_modules aus git entfernt


## [1.0.1] - [1.0.8]
- Inhalte + Titel der USB-Card als pdf hinzugefügt
- mk-package: neue Version nun als Parameter übergebbar
- mk-package: zeigt nun alte Versionsnummer an, wenn keine angegeben wurde
- Sabine Lochner aus Standesführung entfernt
- Div. Layout- und Syntaxfehler korrigiert


## [0.14.0] - [1.0.0]
- Neuer Textteil Allgemeinmedizin von Fr. Matzek
- Textkorrekturen v. Herrn Koller
- Bild von T.Wizany zur Schilderordnung
- Kleine typografische Anpassungen
- Willkommen: Kontaktmöglichkeit Haas, Schnöll, Gonzalez
- Korrekter Link zu DFP-Fortbildung-Artikel
- Sparkasse: BP -> Sonderkonditionen, Logo kleiner
- Link zu Formular Aufnahme in die Vertreterliste eingefügt
- Feedback Mailadresse, Foto Buchmayer
- Footer-Text: CSS-Anpassung
- Fließtext im Blocksatz
- Berufshaftpflicht-Antrag: Link-update
- Logo Bildingspartnerschaft besser platziert
- grubinger: besseres Titel-Layout
- 'user' -> 'Benutzername' in credentials
- Versionierung auf Deutsch geändert

## [0.14.0]
- Verlinkung ausbildungsreform 2015 direkt auf die Homepage
- Seite Bildungspartnerschaft erstellt
- Korrektur Buchmayer in Haftpflicht Griessner hinzugefügt
- Link auf Wohlfahrtsfonds bereinigt
- Besseres Layout für Servicestelle/Adresse
- Georg Fuchs hinzugefügt Update Seite Medien/Infos für Ärzte
- Link to Christian Gonzalez /Realisation USBCard
- USBCreator aus git entfernt - wird separates Projekt

## [0.13.0]
- +fontawesome

## [0.12.0]
- better username/pw layout
- added some graphics to USBCreator, basic layout
- move credential-area to the front
- open download links in new tab
- jquery.min.js -> jquery.js in config
- First Version of (plain) USBCreator in QML - stub.
- Better Fax number layout (one line) in footer
- +Reihungsrichtlinien
- replaced minified jquery with full for better debugging
- many fixes and additions

## [0.10.3]
- added Downloads and Ansprechpartner to ausbildungsreform2015
- fixed img height of persons photos
- remove "Ärztekammern" link on footer (comlicated menu)
- fixed "willkommen" page anchor make mk-package script tag the git 
  commit with current version

## [0.9.0]
- add portlet "downloads",
- "fixed" right angle before footer links - took a ">" instead of \f105
- Vorwort Haas better text flow/layout
- "nav" and "persons" as separate blocks. now possible to use separate persons
  for each page.
- added persons riss, grubinger

## [0.8.0]
- HUGE change: added backbone.js to serve everything from a single html file
- added some images to fix e.g. the quotation mark on portlet headers
- added coffeescript
- correct mk-package not finding subfolders
- update, added persons, photos, haftpflicht text change etc.
- latest update to new content. (rechtliches, allgemeines, some layout changes)

## [0.6.0]
- ignore usbard*.zip file in git
- added person lochner
- added package creation script: mk-package.sh
- added all images of people to git

## [0.5.0]
- don't add ".html" to external links in portlet-nav
- enable level 3 nav
- 338ea8a don't add ".html" to external links in portlet-nav
- added "Facharzt"
- add "Arzt für Allgemeinmedizin"
- table instead of ul. this is a workaround for a bug in the aeksbg CMS
- add forstner tel, gonzalez tel, format correction
- many small content/typo fixes
- typo: "Gertrude" -> "Gertrud" Sommerer
- willkommen overhaul
- add image mixin into layout for general availability
- auto update current year in footer footer layout corrections
- change google maps -> openstreetmap link
