## Ärztekammer - USB-Stick

Diese Software enthält den Quellcode für den USB-Stick, den alle Ärzte bei
Ihrem Erstkontakt in der Ärztekammer Salzburg erhalten. Prinzipiell beinhaltet 
er die abgespeckte Homepage der ÄK Salzburg, die durch node.js lokal verfügbar
gemacht wurde.

### Installation:

#### Voraussetzungen

Grundvoraussetzung ist ein funktionierendes node.js auf einem Linux-system,
empfohlen wird 
Ubuntu LTS, z.B: 16.04. Daruf sollte vorerst der node.js-Package-Manager *npm*
installiert werden. Es ist nicht notwendig, die neueste Version zu installieren,
Da sicherheitstechnische Aspekte irrelevant sind: Die statisch generierte
Seite ist später auf einem USB-Stick, und hat kein Sicherheitsrisiko zu decken.
Git wird zur Versionsverwaltung mit installiert.

    sudo apt install npm nodejs-legacy git 

Um Änderungen per git auch wieder einpflegen zu können, ist es notwendig, einen
[Gitlab](https://gitlab.com)-account und Schreibrechte am Projekt 
aek-stick zu besitzen.
Weiters sollte man git eingerichtet haben:

    git config --global user.name <Vorname Nachname>
    git config --global user.email <emailadresse>

#### Einrichten der SSH-Schlüssel

Um auf GitLab schreiben zu können, muss dort ein Benutzer angelegt werden. Dann 
muss unter Ubuntu ein SSH-Schlüssel erzeugt werden, am einfachsten ist das im 
Terminal mit dem Befehl `ssh-keygen` (Fragen einfach per [Enter] bestätigen.)
Dann bitte den Inhalt der Datei `~/.ssh/id_rsa.pub` in Gitlab bekannt machen, 
indem man [hier](https://gitlab.com/profile/keys) den Schlüssel hinzufügt.
Damit ist man berechtigt, schreibend auf das Repository zuzugreifen 
(vorausgesetzt) der Inhaber hat deinen Gitlab-Benutzer freigeschaltet.

#### Lokale Installation des aek-usbstick

    git clone git@gitlab.com:nerdoc/aek-stick.git
    cd aek-stick/stick
 
Zur Entwicklung wird [Brunch](http://brunch.io/) verwendet. Dieses bietet
eine automatisierte Build-Umgebung. Es wird mit allen anderen benötigten
nodejs-Modulen installiert mittels:

    npm install

Damit wird Brunch und alle verwendeten Abhängigkeiten lokal unter
stick/node_modules installiert.

Brunch sollte hier nun sauber laufen. Ein kurzer Test zeigt sofort ob alles 
funktioniert:

    npm start

sollte einen Server auf *http://localhost:3333* starten. Wenn man möchte, dass 
nicht nur von localhost, sondern auch von anderen Rechnern im Netzwerk auf den
Entwicklungs-Webserver zugegriffen werden kann, startet man brunch mittels

    ./brunch watch --server --network

oder kurz

    ./brunch w -s -n

### Mitarbeit

Zur Mitarbeit bzw. zum Editieren des Inhalts siehe [CONTRIBUTING.md](CONTRIBUTING.md)

### Lizenz und Verwendung

* Der Code dieser Applikation ist unter der [AGPL](https://www.gnu.org/licenses/agpl-3.0.html) lizensiert, siehe die Datei [LICENSE](LICENSE). 
D.h. Die Applikation zum Erstellen eines Sticks kann grundsätzlich frei
verwendet werden. Das betrifft jedoch nicht die verwendeten Bilder und Texte,
siehe folgend.
* Der Code der verwendeten Bibliotheken (Node.js, Brunch, CoffeeScript,
Nug, jQuery, BackBone.js u.v.m.) ist unter der Lizenz der jeweiligen Projekte einzusehen.
* Die verwendeten Bilder sind NICHT frei verfügbar, sondern unterliegen dem 
Urheberrecht, und dürfen NICHT in abgeleiteten Werken ohne Genehmigung der
Urheber (z.B. Ärztekammer Salzburg) verwendet werden.
* Die verwendeten Texte sind urheberrechtlich geschützt und Eigentum der 
Ärztekammer Salzburg bzw. der jeweiligen Autoren, und dürfen ohne Genehmigung
NICHT in anderen Projekten verwendet werden.