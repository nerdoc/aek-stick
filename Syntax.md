# Syntax


## Blöcke

Hier eine Kurze Übersicht, wie man den Stick editieren kann.

Jede Datei beginnt mit einer `extends` Direktive, die das Template angibt. 
Gundsätzlich sollte hier immer

    extends _layout

als erste Zeile verwendet werden, das ist das Grundlayout des Sticks.
Layouts können aber vererbt werden:

    _layout.pug
      ↳ _aus_fortbildung.pug
        ↳ aus_fortbildung.allgemeinmedizin.pug

Templates sollten immer mit einem Unterstrich `_` beginnen. Seiten sind dann
normale Dateinamen.

In den Templates sind Blöcke definiert, die dann in den Dateien überschrieben
werden können. Jeder Block beginnt mit dem tag `block`, folgende Blöcke
stehen zur Auswahl:

    block header    // header, sollte nicht verändert werden
    block nav       // rendert ein Navigationsmenü
    block persons   // rendert einen Personenblock rechts
    block data      // Daten, z.B. Dateianhänge etc.
    block column-1  // Haupt-Content links, mind. 1144px Breite
    block column-2  // Spalten-Content rechts unter allem anderen. 
                    // Sollte möglichst wenig verwendet werden
                    // oder nur STATT persons/data

In der Zieldatei kann einfach mittels

    block data
      :marked
         Das ist ein Inhalt, der im "data"-Bereich gerendert wird

der *data*-Block überschrieben werden. Möchte man den Block nicht ersetzen, 
sondern nur an den bestehenden Inhalt anhängen, ist `block append data` zu
verwenden.

## Portlets, Images und Sections

In [Pug](https://pugjs.org) kann man "Kürzel" definieren, denen man Parameter übergeben kann. Diese
werden dann als gerendert. Damit kann man häufige Muster wieder verwenden.
Das wird für Personen verwendet, Personen-Images, Sections, und sogenannte 
Portlets, die visuelle Blöcke auf der Seite darstellen.

## Personen

Alle Personen, auf die referenziert werden soll, müssen zuerst in der Datei
`/app/models/persons/persons.js` angelegt werden, mit folgendem Schema:

    nachname = 
      name: 'Vorname Nachname'
      email: 'nachname@aeksbg.at'
      tel: '0662/871327-000'
      img: 'nachname.jpg'

Das Bild der Person muss im Ordner `stick/app/assets/images/` vorhanden sein.
Danach kann diese Person in allen anderen Orten mit einem append-Block 
per <nachname> referenziert werden:

    block column-1
      :marked
        Das ist ein *MarkDown*-Text, der als HTML gerendert wird.
        
Das wird dann als "Portlet" in der Hauptansicht links gerendert.

### Bilder einer Person

Es existiert eine `image`-Direktive, die einfach zu verwenden ist. Im 
Fließtext kann einfach

    +image(nachname [, right:boolean] )

verwendet werden. `nachname` ist dabei die variable mit der Person, die in 
`app/models/persons/persons.js` definiert wurde). Deren Bilddateian wird an dieser
Stelle eingebunden, **linksgefloated**, mit einer Standardgröße. 
Möchte man sie rechts haben, einfach

    +image(nachname, true)

angeben.

### Personengruppen und Personen

Personen, die mit einem Bild, Telefonnummer etc. irgendwo auf der Seite 
aufscheinen sollen, müssen zuerst angelegt werden. Dazu editiert man 
`app/models/persons/persons.js`
#### +portlet_persongroup("Betreuung Jungärzte")

Diese Personengruppen werden in der rechten Spalte angezeigt, und können 
einen Titel haben.

    block append persons
      +portlet_persongroup("Betreuung Jungärzte")
        +portlet_person("jungaerzte1", sommerer)
        +portlet_person("jungaerzte2", koller)
      +portlet_persongroup("Kurie Angestellte Ärzte")
        +portlet_person("kurieangestellte", haas)

Hier wird ein Block dem bestehenden "persons"-Block angehängt, und 2
`portlet_persongroup's erzeugt mit den jeweiligen Titeln und Portlets mit
Personen als Inhalt.

#### +portlet_person(id, person, headertext)

 * id: tag-id des HTML, damit später das Element einfach wieder gefunden werden
   kann, z.B. "jungaerzte1"
 * headertext: text in header über Foto, z.B. "Betreuung Jungärzte"
 * person: Js-Element der Person (definiert in persons.pug).
   Deren *name*, *email*, *tel*, *fax*, *img* werden automatisch gerendert. Falls
   diese leer sind wirds nicht gerendert.

## Sections

#### +section-content(title)

Erstellt eine Section mit beliebigem Inhalt. typischerweise wird der Inhalt
MarkDown-Text sein:

    +section-content("Allgemeines")
      :marked
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
        diam nonumy eirmod tempor invidunt ut labore et dolore magna
        aliquyam erat, sed diam voluptua. At vero eos et accusam et justo
        duo dolores et ea rebum. Stet clita kasd gubergren, no sea.

## Navigation

Diese sollte, um das Layout zur ÄKS-Homepage zu wahren, im Block *nav* sein. 
Dazu erstellt man in seinem Template einen Block, der den Block *nav* 
überschreibt:

    block nav

Danach (oder davor) müssen die Menüpunkte definiert werden. 
Dazu generiert man ein JSON-Array mit folgendem Inhalt:

    [
      { 
        text: "Menüpunkt 1", 
        link: "allgemeines.html",
        items: [...],    // Untermenüs
      },
      {...},
    ]

Am einfachsten erstellt man also mehrere Variablen:

    - var menu_eins = { text: "Erster Menüpunkt", link: "erstes_menu" }
    - var menu_unter = { text: "Untermenü", link: "unterpunkt" }
    - var menu_zwei = { text: "Zweiter Menüpunkt", link: "zweites_menu", items: [menu_unter] }
    
    - var nav = [menu_eins, menu_zwei]

Der Parameter `text` ist die Bezeichnung des Menüpunkts. `link` bezeichnet
die .pug-Datei, in der das Template liegt (ohne Pfad, ohne ".pug"). 

Danach wird noch das Portlet `portlet-nav` mit dem Menü erstellt zum Rendern:

#### +portlet-nav(menu, title)

Erstellt ein Portlet mit einer Navigation. Das oben erstellte Menü wird dem
Portlet einfach als Parameter übergeben. Das wird dann gerendert in

    Bar
    Bazz
      ↳ Foo

#### +portlet_download(title)

Erstellt einen Downloadbereich in der rechten Spalte (mit optionalem Titel).
Darin können weitere Elemente plaziert werden:

    +portlet_download(title)
      +download_file(title, link, type)
        Irgendeine Beschreibung

`title`, `link` ist selbsterklärend. `type` ist die Dateiendung.
Falls im Ordner /images eine Datei namens <type>.png existiert, wird diese
zum Link angezeigt (z.B. wenn "pdf" angebenen wird, sucht das Skript nach
der Datei "pdf.png". Der Blockinhalt unterhalb von +download_file selbst 
wird als Beschreibung unterhalb des Dateilinks plaziert.

#### TL;DR
Für alle denen das Lesen der ganzen Beschreibung zu lange dauert, hier ist ein
Grundgerüst zum Kopieren, das für eine neue Datei verwendet werden kann:

    extends _layout
    
    block column-1
      +image(forstner)
      +image(ebner, true)
      :marked
        Das ist ein *MarkDown*-Text, der als HTML gerendert wird. Links davon 
        kommt ein Bild. Rechts auch.
    
    block nav
      - var menu_eins = { text: "Erster Menüpunkt", link: "erstes_menu" }
      - var menu_unter = { text: "Untermenü", link: "unterpunkt" }
      - var menu_zwei = { text: "Zweiter Menüpunkt", link: "zweites_menu", items: [menu_unter] }
      //- und nun  zusammengefasst:
      - var nav = [menu_eins, menu_zwei]
      
      +portlet-nav(menu, "Optionaler Titel")
    
    block persons
      +portlet_persongroup("Betreuung Jungärzte")
        +portlet_person("jungaerzte1", sommerer)
    
    block downloads
      +portlet_download(title)
        +download_file(title, link, type)
          Irgendeine Beschreibung
