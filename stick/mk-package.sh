#!/bin/sh

usage() {
  cat << EOF
Usage:
  ${0##*/} <version>

  Creates a zipped package of the software, ready to be extracted
  onto an USB stick.

Parameters:
  version     Version number to use for the new package.
              This should be in the format "x.y.z", like "1.0.2"
              The last version was: $oldversion

EOF
}

log_error() {
  echo "  ERROR: $1"
}

die() {
    test "$1" != ""  && echo $1
    exit 1
}

brunch_build() {
    echo "deleting public content..."
    rm -rf public/*

    echo "executing brunch..."
    ./brunch build --production || die "Could not create build using brunch! Please check."
}

create_zip() {
    echo "creating zip file..."
    cd public
    zip -r -9 "../usbcard-$newversion.zip" * > /dev/null
    cd ..
}

check_prerequisites() {

    brunch=$(which brunch)

    # look for editor
    if [ ! -x "$EDITOR" ]; then
        EDITOR=$(which nano)
    fi

    # check for brunch executable
    if [ "$brunch" = "" ]; then
        if [ -x ./node_modules/brunch/bin/brunch ]; then
            brunch="./node_modules/brunch/bin/brunch"
        else
            die "'brunch' executable not found. Exiting."
        fi

    fi

    # if there are changes to the git repo, exit with error. Don't let us automatically commit
    # half-brewn features during the version update commit.
    test "`git status --porcelain`" = "" || die "There are changes in this repository. Please commit them first or remove them."

    # check if we are in the root directory of the stick
    if [ ! -f brunch-config.coffee ]; then
        log_error "Wrong directory! Please start in the 'stick' directory."
        exit
    fi

    # check if first parameter is empty (should be a version number)
    if [ "$newversion" = "" ]; then
        log_error "Please specify version number as parameter."
        usage
        die
    fi
    echo "Old version number: $oldversion"
    echo "New version number: $newversion"
    if git tag | grep -q "$newversion"; then
        log_error "There already exists a git tag with version '$newversion'."
        echo "  Exiting."
        exit 2
    fi
}


####### MAIN SECTION #######

oldversion=`cat package.json |grep version | cut -d '"' -f4`
newversion=$1

check_prerequisites

brunch_build


# moving to a new git branch, to easily rollback in case of errors.
git checkout -b "$newversion"
echo "adding new version number to git, add tag $newversion..."

# saving version number into an include file
echo "- var version = '$newversion';" > app/templates/version.pug

# fill in new version number into package.json variable
sed -i "s/\"version\": *\".*\",/\"version\": \"$newversion\",/" package.json

# create a tmp user-editable "commit" file
tmpfile_commit_text=`mktemp`
cat > $tmpfile_commit_text << EOF


# Bitte Beschreiben Sie hier (oberhalb) kurz, welche Änderungen seit dem letzten Release vorgenommen wurden.
# WICHTIG: Jede Änderung bitte in EINE Zeile!
# Als Hilfe sehen Sie hier eine Auflistung der Änderungen aus dem Gitlog seit der Version $oldversion:
#
EOF
git log --oneline $oldversion..HEAD | sed "s/^\(.*\)$/# \1/" >> $tmpfile_commit_text

# let the user edit the commit text
$EDITOR $tmpfile_commit_text

# create a tmp changelog file to insert into CHANGELOG.md
tmpfile_changelog=$(mktemp)
IFS='' egrep -v "^ *#.*$|^ *$" $tmpfile_commit_text | sed "s/\(.*\)/- \1/" > $tmpfile_changelog

# if user did provide any text, use that as Changelog.
if [ -s $tmpfile_changelog ]; then

    printf "\n## [$newversion] - $(date +%F)\n" | cat - $tmpfile_changelog > tmp_changelog.txt
    mv tmp_changelog.txt $tmpfile_changelog

    # insert new changelog after "Änderungen:"
    sed -i "/^Änderungen: *$/r $tmpfile_changelog" ../CHANGELOG.md
    rm -f $tmpfile_changelog

    git diff
    read -p "Should this be committed and pushed to the server? [y/N]" yn
    if [ "$yn" = "y" ]; then
        git add ../CHANGELOG.md
        git add app/templates/version.pug
        git add package.json
        git commit -m "Versionsnummer auf $newversion erhöht." -q
        git tag $newversion
        git checkout master -q
        git merge "$newversion"
        git push
        # push tag to server
        git push origin $newversion
        create_zip
    else
        git checkout master -q
        git checkout package.json
        git checkout app/templates/version.pug
        git checkout ../CHANGELOG.md
    fi


else
    echo "Commit aufgrund leerer Beschreibung abgebrochen."
    git reset package.json
    git reset app/templates/version.pug
    git checkout master -q
fi

# finally, clean up:

rm -f $tmpfile_commit_text

# remove $newversion branch
git branch -D "$newversion"
