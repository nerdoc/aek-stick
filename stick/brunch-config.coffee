exports.config =
  # See http://brunch.io/#documentation for docs.
#  conventions:
#    assets: /^app\/assets\//
  files:
    javascripts:
      joinTo:
        'data/data.js': "app/persons.js"
        'data/app.js': /^app/
        'data/vendor.js': /^vendor/
      order:
        before: [
          'vendor/scripts/jquery.js',
          'vendor/scripts/underscore.js',
        ]
    stylesheets:
      joinTo:
        'data/app.css': /^app\/styles/
    templates:
      joinTo:
        'data/app.js': /^app\/templates/

  plugins:
    jadePages:
      # ignore files starting with '_'
      pattern: /^app\/[^_].*\.jade$/
      htmlmin: false
