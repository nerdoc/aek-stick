
// 
// Liste aller verwendeten Personen
// 
// Neue Personen hinzufügen wie folgt:
// 
// nachname = {
//   name: "<Vorname> <Nachname>",
//   email: "<email>@aeksbg.at",
//   tel: "0662/871327-<Durchwahl>",
//   fax: "0662/871327-10",
//   img: "<nachname>.jpg" 
// }
// 
// Die Person kann dann überall, zB. im "block person", oder mit "+image(nachname)" verwendet werden.
// 

bittner = {
  name: 'Dr. Reinhard Bittner',
  email: 'reinhard.bittner@gmx.at',
  tel: '0660/1004400',
  img: 'bittner.jpg'
};

boehm = {
  name: 'Helmut Böhm',
  email: 'boehm@aeksbg.at',
  tel: '0662/871327-120',
  fax: '0662/871327-10',
  img: 'boehm.jpg'
};

brandl = {
  name: 'Jakob Brandl',
  email: 'brandl@aeksbg.at',
  tel: '0662/871327-141',
  fax: '0662/871327-10',
  img: 'brandl.jpg'
};

buchmayer = {
  name: 'Gerald Buchmayer',
  email: 'gerald.buchmayer@pbp.at',
  tel: '0662/430 966',
  img: 'buchmayer.jpg'
};

feil = {
  name: 'Mag. Isabell Feil',
  email: 'feil@aeksbg.at',
  tel: '0662/871327-126',
  fax: '0662/871327-10',
  img: 'feil.jpg'
};

forstner = {
  name: 'Dr. Karl Forstner',
  tel: '0662/871327-112',
  email: 'forstner@aeksbg.at',
  img: 'forstner.jpg'
};

fuchs = {
  name: 'Georg Fuchs',
  email: 'fuchs@aeksbg.at',
  tel: '0662/871327-137',
  fax: '0662/871327-10',
  img: 'fuchs.jpg'
};

griessner = {
  name: 'Gerhard Griessner',
  email: 'gerhard.griessner@salzburg.sparkasse.at',
  tel: '050100-47221',
  img: 'griessner.jpg'
};

grubinger = {
  name: 'Andrea Grubinger',
  email: 'grubinger@aeksbg.at',
  tel: '0662/871327-127',
  fax: '0662/871327-10',
  img: 'grubinger.jpg'
};

haas = {
  name: 'Dr. Johannes Haas',
  email: '',
  tel: '',
  fax: '',
  img: 'haas.jpg'
};

heindl = {
  name: 'Alexandra Heindl',
  email: 'heindl@aeksbg.at',
  tel: '0662/871327-133',
  fax: '0662/871327-10',
  img: 'heindl.jpg'
};

gonzalez = {
  name: 'Dr. Christian González',
  email: 'christian.gonzalez@nerdocs.at',
  tel: '0650/7644477',
  img: 'gonzalez.jpg'
};

koller = {
  name: 'Wolfgang Koller',
  email: 'koller@aeksbg.at',
  tel: '0662/871327-133',
  fax: '0662/871327-10',
  img: 'koller.jpg'
};

lechner_schedler = {
  name: 'Nicole Lechner-Schedler',
  email: 'lechner-schedler@aeksbg.at',
  tel: '0662/871327-138',
  fax: '0662/871327-10',
  img: 'lechner-schedler.jpg'
};

lochner = {
  name: 'Sabine Lochner',
  email: 'lochner@aeksbg.at',
  tel: '0662/871327-128',
  fax: '0662/871327-10',
  img: 'lochner.jpg'
};

matzek = {
  name: 'Claudia Matzek',
  email: 'matzek@aeksbg.at',
  tel: '0662/871327-112',
  fax: '0662/871327-10',
  img: 'matzek.jpg'
};

pagitsch = {
  name: 'Dr. Sebastian Pagitsch',
  email: 'sebastian@pagitsch.net',
  img: 'pagitsch.jpg'
};

riss = {
  name: 'Renate Riß',
  email: 'riss@aeksbg.at',
  tel: '0662/871327-125',
  fax: '0662/871327-10',
  img: 'riss.jpg'
};

ruhland = {
  name: 'Mag. Conny Ruhland',
  email: 'ruhland@aeksbg.at',
  tel: '0662/871327-115',
  fax: '0662/871327-10',
  img: 'ruhland.jpg'
};

schoepp = {
  name: 'Birgit Schöpp',
  email: 'aeksbg@aeksbg.at',
  tel: '0662/871327-0',
  fax: '0662/871327-10',
  img: 'schoepp.jpg'
};

sommerer = {
  name: 'Gertrud Sommerer',
  email: 'sommerer@aeksbg.at',
  tel: '0662/871327-151',
  fax: '0662/871327-10',
  img: 'nobody.jpg'
};

stadlbauer = {
  name: 'Julia Stadlbauer',
  email: 'stadlbauer@aeksbg.at',
  tel: '0662/871327-119',
  fax: '0662/871327-10',
  img: 'stadlbauer.jpg'
};

vavrovsky = {
  name: 'Dr. Matthias Vavrovsky',
  email: 'matthias.vavrovsky@gmx.at',
  img: 'vavrovsky.jpg'
};

wiesner = {
  name: 'Ana Wiesner',
  email: 'wiesner@aeksbg.at',
  tel: '0662/871327-151',
  fax: '0662/871327-10',
  img: 'wiesner.jpg'
};

zilavec = {
  name: 'Mag. Ronald Zilavec',
  email: 'zilavec@aeksbg.at',
  tel: '0662/871327-220',
  fax: '0662/871327-10',
  img: 'zilavec.jpg'
};
