var PageView;

writeCredentials = function() {
    $("#username").html(username);
    $("#password").html(password);
};

module.exports = PageView = Backbone.View.extend({
    el: 'body',
    template: null,
    initialize: function(options) {
        this.template = require(options.template);
    },
    render: function() {
        this.$el.html(this.template({
            currentRoute: Backbone.history.fragment
        }));
        writeCredentials();
        return this;
    }
});
