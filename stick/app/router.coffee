PageView = require('views/page');

module.exports = Router = Backbone.Router.extend
  willkommen: ->
    (new PageView({template: 'templates/willkommen'})).render()

  routes:
    '': 'willkommen'
    'willkommen': 'willkommen'


# Menü "ALLGEMEINES""
    'allgemeines': ->
      (new PageView({template: 'templates/allgemeines'})).render()
      
    'allgemeines.meldeverpflichtungen': ->
      (new PageView({template: 'templates/allgemeines.meldeverpflichtungen'})).render() 
      
    'allgemeines.aek_aufgaben': ->
      (new PageView({template: 'templates/allgemeines.aek_aufgaben'})).render() 
      
    'allgemeines.haftpflicht': ->
      (new PageView({template: 'templates/allgemeines.haftpflicht'})).render() 
      
    'allgemeines.wohlfahrtsfonds': ->
      (new PageView({template: 'templates/allgemeines.wohlfahrtsfonds'})).render() 
      
    'allgemeines.sonderkonditionen': ->
      (new PageView({template: 'templates/allgemeines.sonderkonditionen'})).render() 
      
    'allgemeines.medien': ->
      (new PageView({template: 'templates/allgemeines.medien'})).render() 

    'allgemeines.reihung': ->
      (new PageView({template: 'templates/allgemeines.reihung'})).render() 


# Menü "ARZT & RECHT"
    'rechtliches.ta': ->
      (new PageView({template: 'templates/rechtliches.ta'})).render()

    'rechtliches.werberichtlinien': ->
      (new PageView({template: 'templates/rechtliches.werberichtlinien'})).render()

    'rechtliches.karenz': ->
      (new PageView({template: 'templates/rechtliches.karenz'})).render()

    'rechtliches.verhaltenskodex': ->
      (new PageView({template: 'templates/rechtliches.verhaltenskodex'})).render()

    'rechtliches.grundvoraussetzungen': ->
      (new PageView({template: 'templates/rechtliches.grundvoraussetzungen'})).render()


# Menü "AUS & FORTBILDUNG"
    'aus_fortbildung': ->
      (new PageView({template: 'templates/aus_fortbildung'})).render()
        
    'aus_fortbildung.aerzteausbildung': ->
      (new PageView({template: 'templates/aus_fortbildung.aerzteausbildung'})).render()
        
    'aus_fortbildung.allgemeinmedizin': ->
      (new PageView({template: 'templates/aus_fortbildung.allgemeinmedizin'})).render()
        
    'aus_fortbildung.facharzt': ->
      (new PageView({template: 'templates/aus_fortbildung.facharzt'})).render()
        
    'aus_fortbildung.ausbildungsreform2015': ->
      (new PageView({template: 'templates/aus_fortbildung.ausbildungsreform2015'})).render()
        
    'aus_fortbildung.ausbildungsstaetten': ->
      (new PageView({template: 'templates/aus_fortbildung.ausbildungsstaetten'})).render()
        
    'aus_fortbildung.rasterzeugnisse': ->
      (new PageView({template: 'templates/aus_fortbildung.rasterzeugnisse'})).render()

    'aus_fortbildung.fortbildung': ->
      (new PageView({template: 'templates/aus_fortbildung.fortbildung'})).render()

    'aus_fortbildung.bildungspartnerschaft': ->
      (new PageView({template: 'templates/aus_fortbildung.bildungspartnerschaft'})).render()


# Menü "FEEDBACK"
    'verbesserungsvorschlag': ->
      (new PageView({template: 'templates/verbesserungsvorschlag'})).render()

