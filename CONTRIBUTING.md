# Mitarbeit beim AEK-USB-Stick

Vorausgesetzt wird eine funktionierende Build-Umgebung, siehe die
[Installationsanleitung](README.md#installation).

Als Template-Sprache wird [Pug](https://pugjs.org) verwendet, diese bietet
einige Vorteile gegenüber HTML und ist leichter lesbar. Für Textinhalt wird
[MarkDown](https://de.wikipedia.org/wiki/Markdown) verwendet, da es sich gut 
lesen lässt und einfach in Pug integrierbar ist.
Vor allem in die Syntax von Pug und MarkDown sollte man sich aneignen, da sie den 
Hauptteil des Inhaltes ausmacht. Javascript/Coffeescript wird nur sehr wenig verwendet.

Als Backend zur Generierung des Sticks kommt [Brunch](https://brunch.io/) zum 
Einsatz, als Router wird [Backbone](http://backbonejs.org/) verwendet. Mit 
diesen beiden sollte man im Normalfall aber nichts zu tun haben.

Die einzelnen Seiten werden dynamisch compiliert und per Skript in eine
ZIP-Datei verpackt, die Versionierung erfolgt dabei automatisch per 
Kommandozeilenparameter.

Als erstes wird die Build-Umgebung gestartet:

    npm start

Danach können alle Dateien editiert werden, die Änderungen werden sofort 
compiliert und der Inhalt am Browser aktualisiert.

Zu den Syntaxregeln gibt es eine [eigene Seite](Syntax.md).
